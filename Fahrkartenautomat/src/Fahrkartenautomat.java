﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {
		
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		
		
		
		do {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		System.out.print(zuZahlenderBetrag+"");
		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println("Neuer Kaufvorgang");
		} while (true);
	}
		
	public static void warte(int millisekunden)	{
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderGesamtbetrag = 0;
		double zuZahlenderBetrag = 0;
		int AnfrageTickets = 0;
		int ihreWahlTickets = 0;
		boolean falscheAuswahl = true;
		boolean nichtbezahlen = true;
		boolean anzahlStimmtNicht = true;
		
		
		
		String[] fahrkartenName = new String[10];
		double[] fahrkartenPreis = new double[10];
		
		fahrkartenName[0] = "Einzelfahrschein Regeltarif AB";
		fahrkartenName[1] = "Einzelfahrschein Regeltarif BC";
		fahrkartenName[2] = "Einzelfahrschein Regeltarif ABC";
		fahrkartenName[3] = "Einzelfahrschein Kurzstrecke";
		fahrkartenName[4] = "Tageskarte Regeltarif AB";
		fahrkartenName[5] = "Tageskarte Regeltarif BC";
		fahrkartenName[6] = "Tageskarte Regeltarif ABC";
		fahrkartenName[7] = "Kleingruppen-Tageskarte Regeltarif AB";
		fahrkartenName[8] = "Kleingruppen-Tageskarte Regeltarif BC";
		fahrkartenName[9] = "Kleingruppen-Tageskarte Regeltarif ABC";
		fahrkartenPreis[0] = 2.9;
		fahrkartenPreis[1] = 3.3;
		fahrkartenPreis[2] = 3.6;
		fahrkartenPreis[3] = 1.9;
		fahrkartenPreis[4] = 8.6;
		fahrkartenPreis[5] = 9.0;
		fahrkartenPreis[6] = 9.6;
		fahrkartenPreis[7] = 23.5;
		fahrkartenPreis[8] = 24.3;
		fahrkartenPreis[9] = 24.9;
		
		
		
		
		while (nichtbezahlen) {
			falscheAuswahl = true;
			System.out.println("Wählen sie ihre Fahrkarte aus:");
			
			for (int i = 0; i < fahrkartenName.length; i++) {
				System.out.println( i+1 + ". " + fahrkartenName[i] + " " + fahrkartenPreis[i] + "0€");
				
			}
			
			System.out.println("11. Möchten sie zahlen?");
			
		while (falscheAuswahl)   {
				ihreWahlTickets = tastatur.nextInt();
				
				if (ihreWahlTickets <= 10 && ihreWahlTickets >=1)  {
			zuZahlenderBetrag = fahrkartenPreis[ihreWahlTickets-1];		
					falscheAuswahl = false;  }
				
				else if (ihreWahlTickets == 11 ) {
					anzahlStimmtNicht = false;
					falscheAuswahl = false;
					nichtbezahlen = false; 
					}
				
				else {
					anzahlStimmtNicht = true;
					System.out.print("Fehler, bitte zahlen sie oder wählen ein Ticket.");
				}
			}
				
		while (anzahlStimmtNicht)	{
					System.out.print("Anzahl der gewünschten Tickets:");
					AnfrageTickets = tastatur.nextInt();
					
					if (AnfrageTickets >= 11)	{
						anzahlStimmtNicht = true;
						nichtbezahlen = false;
						System.out.println("Sie können maximal 10 Tickets kaufen.");
					}
					else if (AnfrageTickets <= 0)  {
						anzahlStimmtNicht = true;
						nichtbezahlen= false;
						System.out.println("Sie können nicht weniger als 1 Ticket kaufen.");
					}
					else {
						break;
					}
					
					
				}
				
				zuZahlenderGesamtbetrag = zuZahlenderGesamtbetrag + (zuZahlenderBetrag * AnfrageTickets);
				zuZahlenderBetrag = 0;
				AnfrageTickets = 0;
				System.out.printf("Momentaner zu Zahlender Betrag: %.2f €\n", zuZahlenderGesamtbetrag);
				
		}
		return zuZahlenderGesamtbetrag; 
		
			
		
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double zuZahlenderBetrag = zuZahlen;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehenderBetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 €): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		return eingezahlterGesamtbetrag;

	}
	
	public static void fahrkartenAusgeben() {
		 
		
				System.out.println("\nFahrschein wird ausgegeben");
				for (int i = 0; i < 8; i++) {
					System.out.print("=");
					warte(500);
				}
				System.out.println("\n\n");

	}
	
	public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rueckgabebetrag;
		
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
				if (rueckgabebetrag > 0.00) {
					System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
					System.out.println("wird in folgenden Münzen ausgezahlt:");

					while (rueckgabebetrag >= 1.99) // 2 EURO-Münzen
					{
						System.out.println("2 EURO");
						rueckgabebetrag -= 2.0;
					}
					while (rueckgabebetrag >= 0.99) // 1 EURO-Münzen
					{
						System.out.println("1 EURO");
						rueckgabebetrag -= 1.0;
					}
					while (rueckgabebetrag >= 0.49) // 50 CENT-Münzen
					{
						System.out.println("50 CENT");
						rueckgabebetrag -= 0.5;
					}
					while (rueckgabebetrag >= 0.19) // 20 CENT-Münzen
					{
						System.out.println("20 CENT");
						rueckgabebetrag -= 0.2;
					}
					while (rueckgabebetrag >= 0.09) // 10 CENT-Münzen
					{
						System.out.println("10 CENT");
						rueckgabebetrag -= 0.1;
					}
					while (rueckgabebetrag >= 0.049)// 5 CENT-Münzen
					{
						System.out.println("5 CENT");
						rueckgabebetrag -= 0.5;
					}
				}

				
			
	}
}
